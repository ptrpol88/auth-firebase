package com.h8.auth_firebase.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import com.h8.auth_firebase.model.CRUD;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
public class CRUDService {
    public CRUD getCRUD(String employeeId) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference documentReference = dbFirestore.collection("crud_user").document(employeeId);
        ApiFuture<DocumentSnapshot> future = documentReference.get();
        DocumentSnapshot document = future.get();

        CRUD crud;
        if(document.exists()){
            crud = document.toObject(CRUD.class);
            return crud;
        }
        return null;
    }

    public String createCRUD(CRUD crud) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionWriteResultApiFuture = dbFirestore.collection("crud_user").document(crud.getEmployeeId()).set(crud);
        return collectionWriteResultApiFuture.get().getUpdateTime().toString();
    }

    public String updateCRUD(CRUD crud) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionWriteResultApiFuture = dbFirestore.collection("crud_user").document(crud.getEmployeeId()).set(crud);
        return collectionWriteResultApiFuture.get().getUpdateTime().toString();
    }

    public String deleteCRUD(String employeeId) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionWriteResultApiFuture = dbFirestore.collection("crud_user").document(employeeId).delete();
        return "Successfully deleted "+ employeeId;
    }
}
