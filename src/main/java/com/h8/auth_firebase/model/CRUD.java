package com.h8.auth_firebase.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CRUD {
    private String employeeId;
    private String name;
    private String job;
}
