package com.h8.auth_firebase.controller;

import com.h8.auth_firebase.model.CRUD;
import com.h8.auth_firebase.service.CRUDService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
public class CRUDController {

    private CRUDService crudService;

    public CRUDController(CRUDService crudService) {
        this.crudService = crudService;
    }


    @GetMapping("/get")
    public CRUD getCRUD(@RequestParam String employeeId) throws InterruptedException, ExecutionException {
        return crudService.getCRUD(employeeId);
    }

    @PostMapping("/create")
    public String createCRUD(@RequestBody CRUD crud) throws InterruptedException, ExecutionException {

        return crudService.createCRUD(crud);
    }

    @PutMapping("/update")
    public String updateCRUD(@RequestBody CRUD crud) throws InterruptedException, ExecutionException {

        return crudService.updateCRUD(crud);
    }

    @PutMapping("/delete")
    public String createCRUD(@RequestParam String employeeId) throws InterruptedException, ExecutionException {

        return crudService.deleteCRUD(employeeId);
    }

    @GetMapping("/test")
    public ResponseEntity<String> testGetEndpoint(){
        return ResponseEntity.ok("Test Get Endpoint is Working");
    }

}
